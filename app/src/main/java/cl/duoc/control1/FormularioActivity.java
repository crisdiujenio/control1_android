package cl.duoc.control1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.Toast;

public class FormularioActivity extends AppCompatActivity {
    private EditText etUser,etPassword;
    private Button btnLogin, btnRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        etUser = (EditText)findViewById(R.id.etUsuario);
        etPassword = (EditText)findViewById(R.id.etPassword);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarFormulario();
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CambiarFormulario();
            }
        });
    }

    private void GuardarFormulario(){
        if(etUser.getText().toString().length()>0 &&
                etPassword.getText().toString().length()>0)
        {

            Toast.makeText(this, "Te has logueado Correctamente", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Ingrese id Usuario y password", Toast.LENGTH_SHORT).show();
        }

    }

    private void CambiarFormulario(){
        Intent I = new Intent(FormularioActivity.this,RegistroActivity.class);
        startActivity(I);
    }
}
