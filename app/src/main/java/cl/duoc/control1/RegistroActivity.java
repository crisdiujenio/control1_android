package cl.duoc.control1;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.view.View;
import android.widget.Toast;

import java.util.Calendar;

public class RegistroActivity extends AppCompatActivity {
    private Button btnFechaNacimiento, btnCrearRegistro;
    private EditText etRegistroNombre,etRegistroApellido, etRegistroTelefono,
            etRegistroMail, etRegistroFechaNacimiento,etRegistroPassword1, etRegistroPassword2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        etRegistroNombre = (EditText) findViewById(R.id.etNombreRegistro);
        etRegistroApellido = (EditText) findViewById(R.id.etApellidoRegistro);
        etRegistroTelefono = (EditText) findViewById(R.id.etTelefonoRegistro);
        etRegistroMail = (EditText) findViewById(R.id.etMailRegistro);
        etRegistroFechaNacimiento = (EditText) findViewById(R.id.etFechaNacimiento);
        etRegistroPassword1 = (EditText) findViewById(R.id.etPasswordRegistro);
        etRegistroPassword2 = (EditText) findViewById(R.id.etPassword2Registro);

        btnCrearRegistro = (Button)findViewById(R.id.btnCrearRegistro);
        btnFechaNacimiento = (Button)findViewById(R.id.btnFechaNacimiento);

        btnFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });

        btnCrearRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarFormulario();
            }
        });
    }

    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etRegistroFechaNacimiento.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }

    private void GuardarFormulario(){
        if(etRegistroNombre.getText().toString().length()>0 &&
                etRegistroApellido.getText().toString().length()>0 &&
                    etRegistroTelefono.getText().toString().length()>0 &&
                        etRegistroMail.getText().toString().length()>0 &&
                            etRegistroFechaNacimiento.getText().toString().length()>0 &&
                                etRegistroPassword1.getText().toString().length()>0 &&
                                    etRegistroPassword2.getText().toString().length()>0)
        {
            if(etRegistroPassword1.getText().toString()!= etRegistroPassword2.getText().toString())
            {
                Toast.makeText(this, "Las contraseñas ingresadas no coinciden", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "Registro creado correctamente", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this, "Favor ingresar toda la informacion", Toast.LENGTH_SHORT).show();
        }

    }
}
